import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AlertService } from './alert.service';

@Injectable()

export class AppInterceptor implements HttpInterceptor {

    constructor(private alertService: AlertService) {

    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        //console.log('intercept');
        const token = localStorage.getItem('token');
        if(token) {
            req = req.clone({
                headers: req.headers.set('x-auth-token', token)
            })
        }
        return next.handle(req);

        // intercept response
        // If status 401 ---> redirect to login and remove token from storage
        // if status 400 ---> this.alertService.isVisible=true; this.alertService.message="message from backend"
    }
}