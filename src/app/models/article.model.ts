export class Articles {
    articles: Article[];
    count: string;
    page: string;
    limit: string;
    pages: string;
}

export class Article {
    _id: string;
    title: string;
    content: string;
    author: string;
    category: Category = new Category();
    isPublished: boolean;
    date: string;
}

export class Category {
    _id: string;
    name: string;
    description: string;
}