import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';
import { Category } from '../models/article.model';
import { AuthService } from '../auth.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  categories: Category[] = [];
  user: User = new User();
  constructor(
    private articlesService: ArticlesService,
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.articlesService.getCategories().subscribe(catagories => {
      this.categories = catagories;
    })
    this.auth.authenticate();
    this.auth.connectedUser$.subscribe(user=> {
      this.user = user;
    })
  }

  logout() {
    this.auth.logout();
  }

}
