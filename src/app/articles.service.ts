import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { Articles, Category, Article } from './models/article.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  public index = 0;
  apiPath = "https://nodejsaycourse.herokuapp.com/api"

  constructor(private http: HttpClient, private auth: AuthService) { }

  getArticles(category="all", page=1) {
    // get articles from backend services ---> GET /api/articles
    return this.http.get<Articles>(this.apiPath + "/categories/" + category + "/articles?page=" + page);
  }

  getArticleById(id) {
    return this.http.get<Article>(this.apiPath + "/articles/" + id);
  }

  addArticle(article) {

    //let headers = new HttpHeaders().set('x-auth-token', localStorage.getItem('token'));

    // post article to backend services ---> POST /api/articles
    return this.http.post(this.apiPath + "/articles", article);
  }
  deleteArticle(id) {
    // delete article with backend services ---> DELETE /api/articles/:id
    if(this.auth.isLoggedIn()) {
      return this.http.delete(this.apiPath + "/articles/"+id);
    }
  }
  updateArticle(id, article) {
    // put article to backend services ---> PUT /api/articles/:id
    return this.http.put(this.apiPath + "/articles/"+id, article);
  }

  getCategories() {
    return this.http.get<[Category]>(this.apiPath + "/categories");
  }
}