import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClassField } from '@angular/compiler';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @Input() article;

  constructor() { }

  ngOnInit() {
  }


}
