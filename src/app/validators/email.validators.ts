import { AbstractControl, ValidationErrors } from '@angular/forms';


export class EmailValidators {
    static shouldBeUnique(control: AbstractControl): Promise<ValidationErrors | null> {
        return new Promise((resolve, reject) => {
            // send request to backend to check if email exist
            setTimeout(()=> {
                if((control.value as string) == "ayariissam@gmail.com") {
                    resolve ({
                        shouldBeUnique: true
                    })
                } else {
                    resolve(null)
                }
            }, 3000)
        });
    }
}