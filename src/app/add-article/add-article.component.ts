import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.scss']
})
export class AddArticleComponent implements OnInit {

  categories = []
  constructor(private articlesService: ArticlesService, private router: Router) { }

  ngOnInit() {
    this.articlesService.getCategories().subscribe(categories => {
      this.categories = categories;
    })
  }

  log(title) {
    console.log(title)
  }

  submit(form) {
    console.log(form);
    if(form.valid) {
      // Send article to backend API
      console.log('form is valid', form.value);
      this.articlesService.addArticle(form.value).subscribe(data=>{
        console.log('Article Added', data);
        this.router.navigate(['/']);
      })

    }else{
      console.log('form is not valid, sorry we can not create article');
    }
  }
}
