import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from '../models/article.model';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.scss']
})
export class EditArticleComponent implements OnInit {

  categories = [];
  id=null;
  article: Article = new Article();
  constructor(private articlesService: ArticlesService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.articlesService.getCategories().subscribe(categories => {
      this.categories = categories;
    })
    this.route.paramMap.subscribe(paramMap=> {
      this.id= paramMap.get("id");
      this.articlesService.getArticleById(this.id).subscribe(article=> {
        this.article = article;
      })
    })
  }

  submit(f) {
    console.log("form", f.value);
    this.articlesService.updateArticle(this.id, f.value).subscribe(data => {
      console.log("data", data);
      this.router.navigate(['/article', this.id]);
    })
  }
}
