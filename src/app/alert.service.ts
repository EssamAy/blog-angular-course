import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  public isVisible = false;
  public message ="this is a sample alert";
  constructor() { }
}
